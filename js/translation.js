/**
extract from:
https://code.google.com/p/jquery-utils/downloads/list

Exemple:
// Translation templates (always starts with english)
$.i18n('en.datePicker', {
    'Month': 'Month',
    'Year':  'Year',
    'Day':   'Day'
});

$.i18n('fr.datePicker', {
    'Month': 'Mois',
    'Year':  'Année',
    'Day':   'Jour'
});

$.i18n('en');
alert($.i18n('datePicker', 'Year')); // returns "Year"

$.i18n('fr');
alert($.i18n('datePicker', 'Year')); // returns "Année"
*/
function getTrans() {
    if (arguments.length == 2) {
        return $.i18n(arguments[0], arguments[1])
    } else if (arguments.length > 2) {
        var str = $.i18n(arguments[0], arguments[1]);
        for (var i=0 ; i < (arguments.length - 2) ; i++) {
            str = str.replace('%s',arguments[(2+i)]);
        }
        return str;
    } else {
        throw new Error('Illegal number of arguments during the call of getTransWithVar');
    }
}
//English Translation
$.i18n('en.js', {
    //Item Frames Rotation
    'fromlabel': 'From Floor',
    'fromtitle': 'check this if you want to see that item rotation applied if the lift is at least at the following level number',
    'fromnumberlabel': 'Floor number',
    'fromnumbertitle': 'if the lift is at least at this floor number the rotation will be applied to the item frames',
    'tolabel': 'To Floor',
    'totitle': 'check this if you want to see that item rotation applied if the lift is at most at the following level number',
    'tonumberlabel': 'Floor number',
    'tonumbertitle': 'if the lift is at most at this floor number the rotation will be applied to the item frames',
    'alerttxt': 'You must at least define an origin or an end floor number for that item rotation.',
    'rotationlabel': 'Rotation',
    'rotationtitle': 'The floor rotation applied for the defined conditions. If the value is 0 the item will point toward the top-right. 1 make it point toward the right while 7 make it point toward the top.',
    'delete': 'Delete definition',
    
    //Floors Calling buttons
    'poslabel': 'Position of the floor %s button',
    'posx': 'The coordinate x for the position of the button existing at the floor %s',
    'posy': 'The coordinate y for the position of the button existing at the floor %s',
    'posz': 'The coordinate z for the position of the button existing at the floor %s',
    'facinglabel': 'Facing',
    'facingtitle': 'The orientation of the button existing at the floor %s',
    'facingdown': 'down',
    'facingup': 'up',
    'facingeast': 'east',
    'facingwest': 'west',
    'facingnorth': 'north',
    'facingsouth': 'south',
    'typelabel': 'Type',
    'typetitle': 'The material of the button existing at the floor %s',
    'typewood': 'wood',
    'typestone': 'stone',
    'textlabel': 'Floor %s text',
    'texttitle': 'This will be added in the text message sent when a player push the button inside the lift. If the player click on that text message the lift will go to that floor.',
    'colorlabel': 'Color',
    'colortitle': 'The color of the floor %s text',
    'colorblack': 'Black',
    'colordark_blue': 'Dark Blue',
    'colordark_green': 'Dark Green',
    'colordark_aqua': 'Dark Aqua',
    'colordark_red': 'Dark Red',
    'colordark_purple': 'Dark purple',
    'colorgold': 'Gold',
    'colorgray': 'Gray',
    'colordark_gray': 'Dark gray',
    'colorblue': 'Blue',
    'colorgreen': 'Green',
    'coloraqua': 'Aqua',
    'colorred': 'Red',
    'colorlight_purple': 'Light Purple',
    'coloryellow': 'Yellow',
    'colorwhite': 'White',
    'colornone': 'None',
    'boldlabel': 'Bold',
    'boldtitle': 'check this to have a bold floor %s text',
    'underlinedlabel': 'Underlined',
    'underlinedtitle': 'check this to have an underlined floor %s text',
    'italiclabel': 'Italic',
    'italictitle': 'check this to have an italic floor %s text',
    'delete': 'Delete Floor'
});
//French translation
$.i18n('fr.js', {
    //Item Frames Rotation
    'fromlabel': 'Depuis l\'étage',
    'fromtitle': 'Cochez cette case si jamais vous souhaitez que la rotation de la flèche dans le cadre s\'applique à partir de l\'étage sélectionné',
    'fromnumberlabel': 'Numéro de l\'étage',
    'fromnumbertitle': 'Si l\'ascenseur est au moins à cet étage, la flèche se déplacera dans les cadres',
    'tolabel': 'Vers l\'étage',
    'totitle': 'Cochez cette case si vous souhaitez que la rotation de la flèche dans le cadre s\'applique dans le cadre si l\'ascenseur est au maximum à l\'étage sélectionné',
    'tonumberlabel': 'Numéro de l\'étage',
    'tonumbertitle': 'si l\'ascenseur est au maximum à cet étage, la flèche bougera en fonction dans les cadres',
    'alerttxt': 'Vous devez déterminer un étage de début et de fin pour que la flèche puisse bouger dans le cadre.',
    'rotationlabel': 'Rotation',
    'rotationtitle': 'La rotation s\'applique pour les conditions définies. Si la valeur est 0, l\'objet pointera en haut à droite. 1 le fera pointer sur le côté à droite. 7 le fera pointer vers le haut.',
    'delete': 'Delete definition',
    
    //Floors Calling buttons
    'poslabel': 'Position du bouton de l\'étage %s',
    'posx': 'Coordonées x de la position du bouton à l\'étage %s',
    'posy': 'Coordonées y de la position du bouton à l\'étage %s',
    'posz': 'Coordonées z de la position du bouton à l\'étage %s',
    'facinglabel': 'Direction',
    'facingtitle': 'Orientation du bouton présent à l\'étage %s',
    'facingdown': 'bas',
    'facingup': 'haut',
    'facingeast': 'est',
    'facingwest': 'ouest',
    'facingnorth': 'nord',
    'facingsouth': 'sud',
    'typelabel': 'Type',
    'typetitle': 'Le matériau du bouton présent à l\'étage %s',
    'typewood': 'bois',
    'typestone': 'pierre',
    'textlabel': 'Texte de l\'étage %s',
    'texttitle': 'Il sera ajouté dans le texte affiché dans le chat lorsque le joueur appuie sur le bouton à l\'intérieur de l\'ascenseur. Si le joueur clique sur ce texte dans le chat, l\'ascenseur ira à cet étage.',
    'colorlabel': 'Couleur',
    'colortitle': 'Couleur du texte de l\'étage %s',
    'colorblack': 'Noir',
    'colordark_blue': 'Bleu foncé',
    'colordark_green': 'Vert foncé',
    'colordark_aqua': 'Bleu foncé',
    'colordark_red': 'Rouge foncé',
    'colordark_purple': 'Violet foncé',
    'colorgold': 'Or',
    'colorgray': 'Gris',
    'colordark_gray': 'Gris',
    'colorblue': 'Bleu',
    'colorgreen': 'Vert',
    'coloraqua': 'Prismarin',
    'colorred': 'Rouge',
    'colorlight_purple': 'Violet clair',
    'coloryellow': 'Jaune',
    'colorwhite': 'Blanc',
    'colornone': 'Aucune',
    'boldlabel': 'Gras',
    'boldtitle': 'Cochez cette case si vous souhaitez avoir le texte de l\'étage %s en gras',
    'underlinedlabel': 'Souligné',
    'underlinedtitle': 'Cochez cette case si vous souhaitez avoir le texte de l\'étage %s de souligné',
    'italiclabel': 'Italique',
    'italictitle': 'Cochez cette case si vous souhaitez avoir le texte de l\'étage %s en italique',
    'delete': 'Supprimer l\'étage'
});