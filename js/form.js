$(function() {
    var tooltips = $( "[title]" ).tooltip({
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" ).addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
            },
            show: {
                effect: "slideDown",
                delay: 250
            },
            hide: {
                effect: "slideUp",
                delay: 250
            }
        }
    });
    $('#advanced-rotation-config').change(function() {
        if(this.checked) {
            $('#advanced-lift-indicators').stop().fadeIn();
            if($('#item-rotation-config-add-button').attr('data-number') === "0") {
               addFloorIndicatorDefinition();
            }
        } else {
            $('#advanced-lift-indicators').stop().fadeOut();
        }
    });
    addFloor();
    addFloor();
    $('input[type="checkbox"]').change();
    $('select').change();
    
    $( "#dialog-save" ).dialog({
        autoOpen: false,
        modal: true,
        minWidth: 600,
        maxWidth: 1000,
        show: {
            effect: "fold",
            duration: 700
        },
        hide: {
            effect: "fold",
            duration: 700
        }
    });
    $( "#dialog-import" ).dialog({
        autoOpen: false,
        modal: true,
        minWidth: 600,
        maxWidth: 1000,
        show: {
            effect: "fold",
            duration: 700
        },
        hide: {
            effect: "fold",
            duration: 700
        }
    });
});
function addFloor() {
    var number = parseInt($('#floor-button-add').attr('data-number'));
    $('#floor-button-add').attr('data-number',(number+1));
    var hideDelete = '';
    if (number < 2) {
        hideDelete = ' style="display:none;"';
    }
    var content = ''+
'                            <div class="row" id="floor'+ number +'-calling-button">'+
'                                <div class="col-lg-6">'+
'                                    <div class="form-group" id="floor'+number+'-button-pos-group">'+
'                                        <label for="floor'+number+'-button-pos-x">'+getTrans('js','poslabel',number)+'</label>'+
'                                        <div class="row">'+
'                                            <div class="col-lg-4">'+
'                                                <input type="text" class="form-control" name="generator[floors]['+number+'][pos][x]" id="floor'+number+'-button-pos-x" placeholder="x" title="'+getTrans('js','posx',number)+'"/>'+
'                                            </div>'+
'                                            <div class="col-lg-4">'+
'                                                <input type="text" class="form-control" name="generator[floors]['+number+'][pos][y]" id="floor'+number+'-button-pos-y" placeholder="y" title="'+getTrans('js','posy',number)+'"/>'+
'                                            </div>'+
'                                            <div class="col-lg-4">'+
'                                                <input type="text" class="form-control" name="generator[floors]['+number+'][pos][z]" id="floor'+number+'-button-pos-z" placeholder="z" title="'+getTrans('js','posz',number)+'"/>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                                <div class="col-lg-6">'+
'                                    <div class="form-group" id="floor'+number+'-button-orientation-group">'+
'                                        <div class="row">'+
'                                            <div class="col-lg-6">'+
'                                                <label for="floor'+number+'-button-orientation">'+getTrans('js','facinglabel')+'</label>'+
'                                            </div>'+
'                                            <div class="col-lg-6">'+
'                                                <select name="generator[floors]['+number+'][orientation]" id="floor'+number+'-button-orientation" title="'+getTrans('js','facingtitle',number)+'">'+
'                                                    <option value="8">'+getTrans('js','facingdown')+'</option>'+
'                                                    <option value="9">'+getTrans('js','facingeast')+'</option>'+
'                                                    <option value="10">'+getTrans('js','facingwest')+'</option>'+
'                                                    <option value="11">'+getTrans('js','facingsouth')+'</option>'+
'                                                    <option value="12">'+getTrans('js','facingnorth')+'</option>'+
'                                                    <option value="13">'+getTrans('js','facingup')+'</option>'+
'                                                </select>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                                <div class="col-lg-6">'+
'                                    <div class="form-group" id="floor'+number+'-button-type-group">'+
'                                        <div class="row">'+
'                                            <div class="col-lg-6">'+
'                                                <label for="floor'+number+'-button-type">'+getTrans('js','typelabel')+'</label>'+
'                                            </div>'+
'                                            <div class="col-lg-6">'+
'                                                <select id="floor'+number+'-button-type" name="generator[floors]['+number+'][type]" title="'+getTrans('js','typetitle',number)+'">'+
'                                                    <option value="minecraft:wooden_button">'+getTrans('js','typewood')+'</option>'+
'                                                    <option value="minecraft:stone_button">'+getTrans('js','typestone')+'</option>'+
'                                                </select>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                        <!-- Floor Text -->'+
'                        <div class="col-lg-12">'+
'                            <div class="row">'+
'                                <div class="col-lg-6 form-group">'+
'                                    <label for="txt-floor'+number+'">'+getTrans('js','textlabel',number)+'</label>'+
'                                    <input type="text" id="txt-floor'+number+'" class="form-control" name="generator[floors]['+number+'][txt]" placeholder="'+number+'" title="'+getTrans('js','texttitle')+'"/>'+
'                                </div>'+
'                                <div class="col-lg-4">'+
'                                    <label for="txt-floor'+number+'-color">'+getTrans('js','colorlabel')+'</label>'+
'                                    <select class="form-control" id="txt-floor'+number+'-color" name="generator[floors]['+number+'][color]" '+
'                                            onchange="$(\'#floor'+number+'ColorPreviewColor\').css(\'background-color\',getCSSHEXFromWord($(this).val()));" title="'+getTrans('js','colortitle',number)+'">'+
'                                        <option value="black" lang="color.color_black">'+getTrans('js','colorblack')+'</option>'+
'                                        <option value="dark_blue" lang="color.color_dark_blue">'+getTrans('js','colordark_blue')+'</option>'+
'                                        <option value="dark_green" lang="color.color_dark_green">'+getTrans('js','colordark_green')+'</option>'+
'                                        <option value="dark_aqua" lang="color.color_dark_aqua">'+getTrans('js','colordark_aqua')+'</option>'+
'                                        <option value="dark_red" lang="color.color_dark_red">'+getTrans('js','colordark_red')+'</option>'+
'                                        <option value="dark_purple" lang="color.color_dark_purple">'+getTrans('js','colordark_purple')+'</option>'+
'                                        <option value="gold" lang="color.color_gold">'+getTrans('js','colorgold')+'</option>'+
'                                        <option value="gray" lang="color.color_gray">'+getTrans('js','colorgray')+'</option>'+
'                                        <option value="dark_gray" lang="color.color_dark_gray">'+getTrans('js','colordark_gray')+'</option>'+
'                                        <option value="blue" lang="color.color_blue">'+getTrans('js','colorblue')+'</option>'+
'                                        <option value="green" lang="color.color_green">'+getTrans('js','colorgreen')+'</option>'+
'                                        <option value="aqua" lang="color.color_aqua">'+getTrans('js','coloraqua')+'</option>'+
'                                        <option value="red" lang="color.color_red">'+getTrans('js','colorred')+'</option>'+
'                                        <option value="light_purple" lang="color.color_light_purple">'+getTrans('js','colorlight_purple')+'</option>'+
'                                        <option value="yellow" lang="color.color_yellow">'+getTrans('js','coloryellow')+'</option>'+
'                                        <option value="white" lang="color.color_white">'+getTrans('js','colorwhite')+'</option>'+
'                                        <option value="none" selected="true" lang="color.color_none">'+getTrans('js','colornone')+'</option>'+
'                                    </select>'+
'                                </div>'+
'                                <div class="col-lg-2">'+
'                                    <br />'+
'                                    <div class="colorPreview" id="floor'+number+'ColorPreviewColor"></div>'+
'                                </div>'+
'                            </div>'+
'                            <div class="row">'+
'                                <div class="col-lg-3">'+
'                                    <label><input type="checkbox" id="txt-floor'+number+'-bold" '+
'                                           name="generator[floors]['+number+'][bold]" title="'+getTrans('js','boldtitle',number)+'"/> '+getTrans('js','boldlabel')+
'                                    </label>'+
'                                </div>'+
'                                <div class="col-lg-4">'+
'                                    <label><input type="checkbox" id="txt-floor'+number+'-italic" '+
'                                           name="generator[floors]['+number+'][italic]" title="'+getTrans('js','italictitle',number)+'"/> '+getTrans('js','italiclabel')+
'                                    </label>'+
'                                </div>'+
'                                <div class="col-lg-4">'+
'                                    <label><input type="checkbox" id="txt-floor'+number+'-underlined" '+
'                                           name="generator[floors]['+number+'][underlined]" title="'+getTrans('js','underlinedtitle',number)+'"/> '+getTrans('js','underlinedlabel')+
'                                    </label>'+
'                                </div>'+
'                            </div>'+
'                        </div>'+
'                        <!-- End of Floor Text -->'+
'                                <div class="col-lg-9 floor'+number+'-delete"'+hideDelete+'></div>'+
'                                <div class="col-lg-3 floor'+number+'-delete"'+hideDelete+'>'+
'                                    <button type="button" class="full-width btn btn-danger" onclick="delFloor();">'+getTrans('js','delete')+'</button>'+
'                                </div>'+
'                                <div class="col-lg-12">'+
'                                    <hr />'+
'                                </div>'+
'                            </div>';    
    if (number > 2) {
        $('div#floors-calling-buttons').find('.floor'+ (number-1) +'-delete').fadeOut();
    }
    $('div#floors-calling-buttons').append(content);
    $("#floor"+ number +"-calling-button").find( "[title]" ).tooltip({
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" ).addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
            },
            show: {
                effect: "slideDown",
                delay: 250
            },
            hide: {
                effect: "slideUp",
                delay: 250
            }
        }
    });
    return number;
}

function delFloor() {
    var number = parseInt($('#floor-button-add').attr('data-number')) -1;
    $('#floor-button-add').attr('data-number',number);
    $('div#floors-calling-buttons').find('div#floor'+ number +'-calling-button').remove();
    if ((number-1) > 1) {
        $('div#floors-calling-buttons').find('div.floor'+ (number-1) +'-delete').fadeIn();
    }
}

function addFloorIndicatorDefinition() {
    var currentNumber = parseInt($('#item-rotation-config-add-button').attr('data-number'));
    $('#item-rotation-config-add-button').attr('data-number',(currentNumber+1));
    var hideDeleteDefinition = '';
    if (currentNumber == 0) {
        hideDeleteDefinition=' style="display:none;"';
    }
    var content=''+
'                        <!-- Floor '+ currentNumber +' ItemFrame Rotation -->'+
'                        <div class="col-lg-12 floor-itemrotation-definition" id="floor'+ currentNumber +'-itemrotation-definition"><hr/>'+
'                            <div class="row">'+
'                                <div class="col-lg-6">'+
'                                    <label>'+
'                                        <input type="checkbox" class="fromfloor-button" name="generator[frames]['+ currentNumber +'][from]" id="floor'+ currentNumber +'from-button" checked onchange="updateFromFloorButton(this.id);" title="'+getTrans('js','fromtitle')+'"/> &nbsp;'+getTrans('js','fromlabel')+
'                                    </label>'+
'                                </div>'+
'                                <div class="col-lg-6 floorfromnumber-div">'+
'                                    <div class="form-group">'+
'                                        <div class="row">'+
'                                            <div class="col-lg-9">'+
'                                                <label for="floorfrom'+ currentNumber +'">'+getTrans('js','fromnumberlabel')+'</label>'+
'                                            </div>'+
'                                            <div class="col-lg-3">'+
'                                                <input type="text" class="form-control" name="generator[frames]['+ currentNumber +'][fromlvl]" id="floorfrom'+ currentNumber +'" placeholder="0" title="'+getTrans('js','fromnumbertitle')+'"/>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                            </div>'+
'                            <div class="row">'+
'                                <div class="col-lg-6">'+
'                                    <label>'+
'                                        <input type="checkbox" class="tofloor-button" name="generator[frames]['+ currentNumber +'][to]" id="floor'+ currentNumber +'to-button" checked onchange="updateToFloorButton(this.id);" title="'+getTrans('js','totitle')+'"/> &nbsp;'+getTrans('js','tolabel')+
'                                    </label>'+
'                                </div>'+
'                                <div class="col-lg-6 floortonumber-div">'+
'                                    <div class="form-group">'+
'                                        <div class="row">'+
'                                            <div class="col-lg-9">'+
'                                                <label for="floorto'+ currentNumber +'">'+getTrans('js','tonumberlabel')+'</label>'+
'                                            </div>'+
'                                            <div class="col-lg-3">'+
'                                                <input type="text" class="form-control" name="generator[frames]['+ currentNumber +'][tolvl]" id="floorto'+ currentNumber +'" placeholder="1" title="'+getTrans('js','tonumbertitle')+'"/>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                                <div class="col-lg-12 fromtofloor-alert" style="display:none;">'+
'                                    <p class="bg-danger text-warning">'+
'                                        '+getTrans('js','alerttxt')+
'                                    </p>'+
'                                </div>'+
'                            </div>'+
'                            <div class="row">'+
'                                <div class="col-lg-12 form-group">'+
'                                    <div>'+
'                                        <div class="row">'+
'                                            <div class="col-lg-3">'+
'                                                <label for="floorrotation'+ currentNumber +'">'+getTrans('js','rotationlabel')+'</label>'+
'                                            </div>'+
'                                            <div class="col-lg-3">'+
'                                                <input type="text" class="form-control" name="generator[frames]['+ currentNumber +'][rotation]" id="floorrotation'+ currentNumber +'" placeholder="3" title="'+getTrans('js','rotationtitle')+'"/>'+
'                                            </div>'+
'                                            <div class="col-lg-3 delete-orientation"'+hideDeleteDefinition+'></div>'+
'                                            <div class="col-lg-3 delete-orientation"'+hideDeleteDefinition+'>'+
'                                                <button type="button" class="full-width btn btn-danger delete-def" onclick="deleteFloorIndicatorDefinition();">'+getTrans('js','delete')+'</button>'+
'                                            </div>'+
'                                        </div>'+
'                                    </div>'+
'                                </div>'+
'                            </div>'+
'                        </div>'+
'                        <!-- End of Floor '+ currentNumber +' ItemFrame Rotation -->';
    if (currentNumber > 0) {
        $('div#floor'+ (currentNumber-1) +'-itemrotation-definition').find('div.delete-orientation').fadeOut();
    }
    $('#itemframes-rotation-definitions').append(content);
    
    $("#floor"+ currentNumber +"-itemrotation-definition").find( "[title]" ).tooltip({
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" ).addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
            },
            show: {
                effect: "slideDown",
                delay: 250
            },
            hide: {
                effect: "slideUp",
                delay: 250
            }
        }
    });
    return currentNumber;
}

function deleteFloorIndicatorDefinition(number) {
    var number = parseInt($('#item-rotation-config-add-button').attr('data-number')) - 1;
    $('#item-rotation-config-add-button').attr('data-number',number);
    $('div#floor'+ number +'-itemrotation-definition').remove();
    if ((parseInt(number)-1) > 0) {
        $('div#floor'+ (parseInt(number)-1) +'-itemrotation-definition').find('div.delete-orientation').fadeIn();
    }
}

function updateFromFloorButton(checkboxId) {
    var checkbox = $('#'+checkboxId);
    var checked = checkbox.prop('checked');
    var definitionDiv = checkbox.parents('div.floor-itemrotation-definition');
    var alert = definitionDiv.find('.fromtofloor-alert');
    alert.stop().fadeOut();
    if(checked) {
        definitionDiv.find('div.floorfromnumber-div').stop().fadeIn();
    } else {
        definitionDiv.find('div.floorfromnumber-div').stop().fadeOut();
        if (definitionDiv.find('input[type="checkbox"].tofloor-button').prop('checked') !== true) {
            alert.stop().fadeIn();
        }
    }
}
function updateToFloorButton(checkboxId) {
    var checkbox = $('#'+checkboxId);
    var checked = checkbox.prop('checked');
    var definitionDiv = checkbox.parents('div.floor-itemrotation-definition');
    var alert = definitionDiv.find('.fromtofloor-alert');
    alert.stop().fadeOut();
    if(checked) {
        definitionDiv.find('div.floortonumber-div').stop().fadeIn();
    } else {
        definitionDiv.find('div.floortonumber-div').stop().fadeOut();
        if (definitionDiv.find('input[type="checkbox"].fromfloor-button').prop('checked') !== true) {
            alert.stop().fadeIn();
        }
    }
}
function getCSSHEXFromWord(w) {
	if (w == "black") return("#000000");
	if (w == "dark_blue") return("#0000B2");
	if (w == "dark_green") return("#14AB00");
	if (w == "dark_aqua") return("#13AAAB");
	if (w == "dark_red") return("#A90400");
	if (w == "dark_purple") return("#A900B2");
	if (w == "gold") return("#FEAC00");
	if (w == "gray") return("#AAAAAA");
	if (w == "dark_gray") return("#555555");
	if (w == "blue") return("#544CFF");
	if (w == "green") return("#5CFF00");
	if (w == "aqua") return("#5BFFFF");
	if (w == "red") return("#FD5650");
	if (w == "light_purple") return("#FD4DFF");
	if (w == "yellow") return("#FFFF00");
	if (w == "white") return("#FFFFFF");
	if (w == "none") return("#FFFFFF");
	return 'none';
}

