/* A position */
var position = function (/*x,y,z|object*/) {
    var x,y,z;
    if (arguments.length == 3) {
        x = arguments[0];
        y = arguments[1];
        z = arguments[2];
    } else if (arguments.length == 1) {
        x = arguments[0].x;
        y = arguments[0].y;
        z = arguments[0].z;
    } else {
        throw new Error("Arguments number invalid to create a position:"+arguments.length);
    }
    if (typeof x === "number") {
        this.x = x;
    } else if (typeof x === "string") {
        this.x = parseInt(x);
    } else {
        throw new Error("x is nor a number nor a string");
    }
    if (typeof y === "number") {
        this.y = y;
    } else if (typeof y === "string") {
        this.y = parseInt(y);
    } else {
        throw new Error("y is nor a number nor a string");
    }
    if (typeof z === "number") {
        this.z = z;
    } else if (typeof z === "string") {
        this.z = parseInt(z);
    } else {
        throw new Error("z is nor a number nor a string");
    }
    
    /* Transform absolute coordinates to relative coordinates */
    this.getRelativeCoordinates = function(origin) {
        if (typeof origin !== "object") {
            throw new Error("Illegal Argument when calling position.getRelativeCoordinates(origin): the origin must be an object of type position");
        }
        try {
            return new position((this.x - origin.x), (this.y - origin.y), (this.z - origin.z));
        } catch (err) {
            throw new Error("Illegal Argument when calling position.getRelativeCoordinates(origin): the origin must be an object of type position. More informations: " + err);
        }
    }
    
    /* Gets a new position after a movement */
    this.move = function(movement) {
        if (typeof movement !== "object") {
            throw new Error("Illegal Argument when calling position.move(movement): the movement must be an object of type position");
        }
        try {
            return new position((this.x + movement.x), (this.y + movement.y), (this.z + movement.z));
        } catch (err) {
            throw new Error("Illegal Argument when calling position.getRelativeCoordinates(origin): the origin must be an object of type position. More informations: " + err);
        }
    }
    
    /* Prints coordinates with spaces (with spaces between) */
    this.print = function() {
        return this.x + ' ' + this.y + ' ' + this.z;
    }
    
    /* Prints coordinates as relative parameters dx=?,dy=?,dz=? */
    this.printRelativeParameter = function() {
        return 'dx=' + this.x + ',dy=' + this.y + ',dz=' + this.z;
    }
    
    /* Prints coordinates as a absolute parameter */
    this.printParameter = function() {
        return 'x=' + this.x + ',y=' + this.y + ',z=' + this.z;
    }
    
    /* Prints coordinates as relatives coordinates (with spaces between) */
    this.printRelative = function() {
        return '~' + this.x + ' ~' + this.y + ' ~' + this.z;
    }
}



/* An Area */
var area = function(pos1,pos2) {
    this.minPos = function (pos1,pos2) {
        var x,y,z;
        if (pos1.x > pos2.x) {
            x=pos2.x;
        } else {
            x=pos1.x;
        }
        if (pos1.y > pos2.y) {
            y=pos2.y;
        } else {
            y=pos1.y;
        }
        if (pos1.z > pos2.z) {
            z=pos2.z;
        } else {
            z=pos1.z;
        }
        return new position(x,y,z);
    }
    this.maxPos = function (pos1,pos2) {
        var x,y,z;
        if (pos1.x > pos2.x) {
            x=pos1.x;
        } else {
            x=pos2.x;
        }
        if (pos1.y > pos2.y) {
            y=pos1.y;
        } else {
            y=pos2.y;
        }
        if (pos1.z > pos2.z) {
            z=pos1.z;
        } else {
            z=pos2.z;
        }
        return new position(x,y,z);
    }
    
    this.toRelative = function(pos) {
        return new area(this.origin.getRelativeCoordinates(pos),this.end.getRelativeCoordinates(pos));
    }
    
    /* Prints area coordinates with spaces (with spaces between) */
    this.print = function() {
        return this.origin.print() + ' ' + this.end.print();   
    }
    
    /* Prints area coordinates as relatives coordinates (with spaces between) */
    this.printRelative = function() {
        return this.origin.printRelative() + ' ' + this.end.printRelative();
    }
    
    pos1 = new position(pos1);
    pos2 = new position(pos2);
    
    this.origin = this.minPos(pos1,pos2);
    this.end = this.maxPos(pos1,pos2);
}