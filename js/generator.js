function textFromObject(/*obj|obj,floorNumber*/) {
    var floorNumber,obj;
    if (arguments.length > 0 && arguments.length < 3) {
        obj = arguments[0];
    } else {
        throw new Error('Illegal number of arguments during the call of textFromObject(obj|obj,floorNumber) function');
    }
    if (arguments.length == 2) {
        floorNumber = parseInt(arguments[1]);
    } 
    var ret= '{"text":"'+obj.txt+'"';
    if (obj.color !== "none") {
        ret += ',"color":"'+obj.color+'"';   
    }
    if (obj.bold === "true") {
        ret += ',"bold":"true"';
    }
    if (obj.italic === "true") {
        ret += ',"italic":"true"';   
    }
    if (obj.underlined === "true") {
        ret += ',"underlined":"true"';
    }
    if (floorNumber !== undefined) {
        ret += ',"clickEvent":{"action":"run_command","value":"/trigger el_tostep set '+floorNumber+'"}'   
    }
    ret+='}';
    return ret;
}
/* Generation script */
function generate() {
    /* Retrieve form informations */
    var form = $('#generator-form').serializeObject();
    
    /* Create needed variables */
    //Entity Name
    var entityName = form.generator.entity;
    
    //Lift Area
    var areaAbsolute = new area(form.generator.zone.pos1,form.generator.zone.pos2);
    
    //Entity Absolute Position
    var entityAbsolute = areaAbsolute.origin;
    
    //Entity Absolute Position When going up
    var entityAbsoluteUp = entityAbsolute.move(new position(0,1,0));
    
    //Entity Absolute Position When going down
    var entityAbsoluteDown = entityAbsolute.move(new position(0,-1,0));
    
    //Area extended of 1 block under and upside the lift to contain the air zone under the lift and the first block of the cable
    var areaExtendedAbsolute = new area(areaAbsolute.origin.move(new position(0,-1,0)),areaAbsolute.end.move(new position(0,1,0)));
    
    var areaExtendedRelative = areaExtendedAbsolute.toRelative(entityAbsolute);
    
    //When the elevator is going up the areaExtended relative coordinates are not the same
    var areaExtendedRelativeUp = areaExtendedAbsolute.toRelative(entityAbsoluteUp);
    
    //When the elevator is going down the areaExtended relative coordinates are not the same
    var areaExtendedRelativeDown = areaExtendedAbsolute.toRelative(entityAbsoluteDown);
    
    //The area end position relative to the entity position
    var endOfAreaRelativeToEntityPos = areaExtendedAbsolute.end.getRelativeCoordinates(entityAbsolute);
    
    //The first cable block outside the areaExtended
    var cableAbsolutePos = new position(form.generator.cable.pos.x,(areaExtendedAbsolute.end.y+1),form.generator.cable.pos.z);
    
    //The first cable block outside the areaExtended relative to the entity position
    var cableRelativePos = cableAbsolutePos.getRelativeCoordinates(entityAbsolute);
    
    //The floor block position
    var floorBlockAbsolutePos = new position(form.generator.floorblock.pos);
    
    //The floor block position relative to the entity position
    var floorBlockRelativepos = floorBlockAbsolutePos.getRelativeCoordinates(entityAbsolute);
    
    //The button inside the lift position
    var liftButtonAbsolutePos = new position(form.generator.button.pos);
    
    //The button inside the lift position relative to the entity position
    var liftButtonRelativePos = liftButtonAbsolutePos.getRelativeCoordinates(entityAbsolute);
    
    //The relative position of the block that is under the lift. Used to know if he has reached is lower position
    var liftMinPosTest = new position(liftButtonAbsolutePos.x,(areaExtendedAbsolute.origin.y-1),liftButtonAbsolutePos.z);
    
    var liftMinRelativePosTest = liftMinPosTest.getRelativeCoordinates(entityAbsolute);
    
    //The area containing the item frames
    var itemFrameAreaAbsolute = new area(form.generator.framezone.pos1,form.generator.framezone.pos2);
    
    //The area containing the item frames relative to the entity position
    var itemFrameAreaRelative = itemFrameAreaAbsolute.toRelative(entityAbsolute);
    
    //Create ItemFrames Rotation default configuration
    var itemFrames;
    if (form.generator.advanced !== undefined && form.generator.advanced.rotation !== undefined) {
        itemFrames = form.generator.frames;     
    } else {
        itemFrames = {
            0: {from: "false", fromlvl: "", to: "true", tolvl: "0", rotation: "3"},
            1: {from: "true", fromlvl: "1", to: "true", tolvl: "1", rotation: "4"},
            2: {from: "true", fromlvl: "2", to: "true", tolvl: "2", rotation: "5"},
            3: {from: "true", fromlvl: "3", to: "true", tolvl: "3", rotation: "6"},
            4: {from: "true", fromlvl: "4", to: "true", tolvl: "4", rotation: "7"},
            5: {from: "true", fromlvl: "5", to: "true", tolvl: "5", rotation: "0"},
            6: {from: "true", fromlvl: "6", to: "true", tolvl: "6", rotation: "1"},
            7: {from: "true", fromlvl: "7", to: "false", tolvl: "0", rotation: "2"}
        };
    }
    
    //Floors
    var floors = form.generator.floors
    
    //Create result
    var arrayCommands = [];
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_tostep_min='+floors.length+'] el_moving 0');
    arrayCommands.push('scoreboard players add @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order=4] el_order 1');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up_min=1] ~ ~ ~ tp @e['+entityName+',c=1,r=1] ~0 ~1 ~0');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up_min=1] ~ ~ ~ clone '+ areaExtendedRelativeUp.printRelative() +' ~ ~-1 ~ replace force');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up_min=1] ~ ~-1 ~ tp @a['+endOfAreaRelativeToEntityPos.printRelativeParameter()+'] ~0 ~1 ~0');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up=0] ~ ~ ~ tp @e['+entityName+',c=1,r=1] ~0 ~-1 ~0');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up=0] ~ ~ ~ clone '+ areaExtendedRelativeDown.printRelative() +' ~ ~-1 ~ replace force');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1,score_el_going_up=0] ~ ~1 ~ tp @a['+endOfAreaRelativeToEntityPos.printRelativeParameter()+'] ~0 ~-1 ~0');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2] el_max_pos 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2] el_min_pos 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2] el_at_step 0');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2,score_el_going_up_min=1] el_min_pos 0');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2,score_el_going_up=0] el_max_pos 0');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2,score_el_going_up_min=1] ~ ~ ~ detect '+cableRelativePos.printRelative()+' '+form.generator.cable.material+' '+form.generator.cable.datavalue+' scoreboard players set @e[type=ArmorStand,name='+entityName+',r=1,c=1] el_max_pos 0');

    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2,score_el_going_up=0] ~ ~ ~ detect '+liftMinRelativePosTest.printRelative()+' air 0 scoreboard players set @e[type=ArmorStand,name='+entityName+',r=1,c=1] el_min_pos 0');

    arrayCommands.push('execute @e[name='+entityName+',score_el_moving_min=1,score_el_order_min=2,score_el_order=2] ~ ~ ~ detect '+floorBlockRelativepos.printRelative()+' '+form.generator.floorblock.material+' '+form.generator.floorblock.datavalue+' scoreboard players set @e[type=ArmorStand,name='+entityName+',r=1,c=1] el_at_step 1');

    arrayCommands.push('scoreboard players add @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_at_step_min=1,score_el_going_up_min=1,score_el_order_min=3,score_el_order=3] el_step 1');
    arrayCommands.push('scoreboard players remove @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=3,score_el_order=3,score_el_at_step_min=1,score_el_going_up=0] el_step 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=3,score_el_order=3,score_el_max_pos_min=1,score_el_going_up_min=1] el_going_up 0');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=3,score_el_order=3,score_el_min_pos_min=1,score_el_going_up=0] el_going_up 1');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_at_step_min=1,score_el_order_min=3,score_el_order=3,score_el_tostep_min=0] ~ ~ ~ scoreboard players operation @e[name='+entityName+',c=1] el_test_tostep = @e[name='+entityName+',c=1] el_tostep');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_at_step_min=1,score_el_order_min=4,score_el_order=4,score_el_tostep_min=0] ~ ~ ~ scoreboard players operation @e[name='+entityName+',c=1] el_test_tostep -= @e[name='+entityName+',c=1] el_step');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_at_step_min=1,score_el_order_min=5,score_el_order=5,score_el_tostep_min=0,score_el_test_tostep_min=0,score_el_test_tostep=0] el_tostep -1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=5,score_el_order=5,score_el_at_step_min=1,score_el_tostep=-1] el_moving 0');
    var tmpStr
    for(var propt in itemFrames){
        tmpStr = 'execute @e[type=ArmorStand,name='+entityName+
            ',score_el_at_step_min=1,score_el_order_min=3,score_el_order=3,';
        var definition = itemFrames[propt];
        var from = (definition.from === "true");
        var to = (definition.to === "true");
        if (from) {
            tmpStr += 'score_el_step_min='+definition.fromlvl;
        }
        if (from && to) {
            tmpStr += ',';
        }
        if (to) {
            tmpStr += 'score_el_step='+definition.tolvl;
        }
        tmpStr += '] ~ ~ ~ execute @e[type=ArmorStand,name='+entityName+'_ORIGIN,c=1] ';
        tmpStr += itemFrameAreaRelative.origin.printRelative();
        tmpStr += ' execute @e[type=ItemFrame,';
        tmpStr += itemFrameAreaRelative.end.printRelativeParameter();
        tmpStr += '] ~ ~ ~ entitydata @e[type=ItemFrame,c=1] {ItemRotation:'+definition.rotation+'}';
        arrayCommands.push(tmpStr);
    }
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_order_min=5,score_el_order=5,score_el_at_step_min=1,score_el_tostep=-1] '+liftButtonRelativePos.printRelative()+' playsound random.orb @a[r=8] ~ ~ ~ 0.8 0.1 0.1');

    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_order_min=5] el_order 0');
    
    for(var propt in floors){
        if (floors.hasOwnProperty(propt)) {
            currentFloor = floors[propt];
            tmpStr = 'execute @e[type=ArmorStand,name='+entityName+'_ORIGIN] ~ ~ ~ detect ';

            //Add the relative position of each floor button
            var floorButtonAbsolutePos = new position(currentFloor.pos);
            var floorButtonRelativePos = floorButtonAbsolutePos.getRelativeCoordinates(entityAbsolute);
            tmpStr += floorButtonRelativePos.printRelative() + ' ' + currentFloor.type + ' ' + currentFloor.orientation;

            tmpStr += ' scoreboard players set @e[type=ArmorStand,name='+entityName+
                ',score_el_moving=0,score_el_order=0,c=1] el_tostep ' + propt;
            arrayCommands.push(tmpStr);
        }
    }
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=1,score_el_starting=1,score_el_tostep_min=0] ~ ~ ~ scoreboard players operation @e[name='+entityName+',c=1] el_test_tostep = @e[name='+entityName+',c=1] el_tostep');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=2,score_el_starting=2,score_el_tostep_min=0] ~ ~ ~ scoreboard players operation @e[name='+entityName+',c=1] el_test_tostep -= @e[name='+entityName+',c=1] el_step');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=3,score_el_starting=3,score_el_tostep_min=0,score_el_test_tostep_min=1] el_going_up 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=3,score_el_starting=3,score_el_tostep_min=0,score_el_test_tostep=-1] el_going_up 0');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=4,score_el_starting=4,score_el_tostep_min=0,score_el_test_tostep_min=1] el_moving 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=4,score_el_starting=4,score_el_tostep_min=0,score_el_test_tostep=-1] el_moving 1');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=4,score_el_starting=4,score_el_tostep_min=0,score_el_test_tostep_min=0,score_el_test_tostep=0] '+liftButtonRelativePos.printRelative()+' playsound random.orb @a[r=8] ~ ~ ~ 0.8 0.1 0.1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_at_step_min=1,score_el_starting_min=4,score_el_starting=4,score_el_tostep_min=0,score_el_test_tostep_min=0,score_el_test_tostep=0] el_tostep -1');
    arrayCommands.push('scoreboard players add @e[type=ArmorStand,name='+entityName+',score_el_starting_min=1,score_el_starting=5] el_starting 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_moving=0,score_el_tostep_min=0,score_el_starting=0] el_starting 1');
    arrayCommands.push('scoreboard players set @e[type=ArmorStand,name='+entityName+',score_el_starting_min=5] el_starting 0');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+'] '+liftButtonRelativePos.printRelative()+' detect ~ ~ ~ '+form.generator.button.type+' '+form.generator.button.orientation+' scoreboard players set @p el_activation 1');
    arrayCommands.push('scoreboard players enable @a[score_el_activation_min=2,score_el_activation=2] el_tostep');
    
    //Create the tellraw command
    tmpStr = 'tellraw @a[score_el_activation_min=2,score_el_activation=2] ["",';
    tmpStr += textFromObject(form.generator.prefix);
    var i=0;
    for(var propt in floors){
        i++;
        tmpStr += ',' + textFromObject(floors[propt],propt);
        if (i !== floors.length) {
            tmpStr += ',' + textFromObject(form.generator.separator);
        }
    }
    tmpStr += ']';
    arrayCommands.push(tmpStr);
    
    arrayCommands.push('scoreboard players set @a[score_el_activation_min=2,score_el_activation=2] el_tostep -1');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving=0,score_el_order=0] ~ ~ ~ execute @a['+endOfAreaRelativeToEntityPos.printRelativeParameter()+',score_el_tostep_min=0,c=1] ~ ~ ~ scoreboard players operation @e[name='+entityName+'] el_tostep = @p[score_el_tostep_min=0] el_tostep');
    arrayCommands.push('execute @e[name='+entityName+',score_el_moving=0,score_el_order=0] ~ ~ ~ execute @a['+endOfAreaRelativeToEntityPos.printRelativeParameter()+',score_el_tostep_min=0,c=1] ~ ~ ~ scoreboard players set @p[score_el_tostep_min=0] el_tostep -1');
    arrayCommands.push('scoreboard players add @a[score_el_activation_min=1] el_activation 1');
    arrayCommands.push('scoreboard players set @a[score_el_activation_min=50] el_activation 0');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=1,score_el_order=1] '+liftButtonRelativePos.printRelative()+' playsound note.hat @a[r=8] ~ ~ ~ 0.2 0.1 0.1');
    arrayCommands.push('execute @e[type=ArmorStand,name='+entityName+',score_el_moving_min=1,score_el_order_min=3,score_el_order=3] '+liftButtonRelativePos.printRelative()+' playsound note.hat @a[r=8] ~ ~ ~ 0.2 0.1 0.1');

    var arrayInit = [];
    
    //Delete old entities to make a reimport work fine
    arrayInit.push('/kill @e[name='+entityName+']');
    arrayInit.push('/kill @e[name='+entityName+'_ORIGIN]');
    
    //create entities
    arrayInit.push('/summon ArmorStand ' + entityAbsolute.print() + ' {CustomName:"' + entityName + '_ORIGIN",Marker:1b,Invisible:1,Invulnerable:1,NoGravity:1,PersistenceRequired:1}');
    arrayInit.push('/summon ArmorStand '+ entityAbsolute.print() +' {CustomName:"' + entityName + '",Marker:1b,Invisible:1,Invulnerable:1,NoGravity:1,PersistenceRequired:1}');
    
    //Create scoreboards
    arrayInit.push('scoreboard objectives add el_starting dummy');
    arrayInit.push('scoreboard objectives add el_test_tostep dummy')
    arrayInit.push('scoreboard objectives add el_tostep trigger');
    arrayInit.push('scoreboard objectives add el_activation dummy');
    arrayInit.push('scoreboard objectives add el_max_pos dummy');
    arrayInit.push('scoreboard objectives add el_min_pos dummy');
    arrayInit.push('scoreboard objectives add el_moving dummy');
    arrayInit.push('scoreboard objectives add el_going_up dummy')
    arrayInit.push('scoreboard objectives add el_step dummy');
    arrayInit.push('scoreboard objectives add el_order dummy')
    arrayInit.push('scoreboard objectives add el_at_step dummy');

    //init scoreboard
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_starting 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_test_tostep -1');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_tostep -1');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_activation 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_max_pos 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_min_pos 1');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_moving 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_going_up 1');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_step 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_order 0');
    arrayInit.push('scoreboard players set @e[type=ArmorStand,name=' + entityName + '] el_at_step 1');
    
    $('#result').html(arrayCommands.join('<br />'));
    $('#result-init').html(arrayInit.join('<br />'));
    for (var i = 0; i<arrayInit.length; i++) {
        arrayInit[i]='INIT:'+arrayInit[i];        
    }
    var strCommands = arrayInit.join('\\n') + '\\n' + arrayCommands.join('\\n');
    var strCompressedCommands = combineCommands(strCommands,'\\n','quartz_block','glass 0');
    $('#result-compressed').html(strCompressedCommands);
    $('#result-compressed-div').fadeIn();
}
