//Error attr('name') txt replacement
function prepareAttrName(name) {
    return name.replace('generator','').replace(/\]\[/g,'.').replace(/\[/g,'').replace(/\]/g,'');
}
//Field Validation
function validateAndGenerate() {
    var error = false;
    var number;
    var numericInputs = $('input[type="text"][name*="[x]"]')
                        .add('input[type="text"][name*="[y]"]')
                        .add('input[type="text"][name*="[z]"]')
                        .add('input[type="text"][name*="[datavalue]"]')
                        .not('input[type="text"][name*="[frames]"]');
    
    var txtInputs = $('input[type="text"]')
                        .not('input[type="text"][name*="[x]"]')
                        .not('input[type="text"][name*="[y]"]')
                        .not('input[type="text"][name*="[z]"]')
                        .not('input[type="text"][name*="[datavalue]"]')
                        .not('input[type="text"][name*="[prefix]"]')
                        .not('input[type="text"][name*="[frames]"]');
    var errors = [];
    
    //TextInputs
    $.each(txtInputs,function(index, input) {
        $(input).parents('.form-group').addClass('has-success');
        $(input).parents('.form-group').removeClass('has-error');
    });
    $.each(txtInputs,function(index, input) {
        if ($(input).val().length == 0) {
            $(input).parents('.form-group').addClass('has-error');
            $(input).parents('.form-group').removeClass('has-success');
            errors.push("Please enter some text in the field " +
                        prepareAttrName($(input).attr('name')));
            
        }
    });
    //Numeric Inputs
    $.each(numericInputs,function(index, input) {
        $(input).parents('.form-group').addClass('has-success');
        $(input).parents('.form-group').removeClass('has-error');
    });
    $.each(numericInputs,function(index, input) {
        if (isNaN($(input).val()) || $(input).val().length == 0) {
            $(input).parents('.form-group').addClass('has-error');
            $(input).parents('.form-group').removeClass('has-success');
            errors.push("Please enter a valid numeric value in the field " +
                        prepareAttrName($(input).attr('name')));
        }
    });
    //Generate or show errors
    if (errors.length==0) {
        $('#generate-button').removeClass("btn-danger").addClass("btn-success");
        $('#validation-error-div').fadeOut();
        generate();
    } else {
        $('#generate-button').removeClass("btn-success").addClass("btn-danger");
        $('#validation-error-div').html(errors.join('<br />')).fadeIn();
    }
}
//Define fields in the form from fields array
function setFormFields(fields) {
    var fieldsNotAddedYet=[];
    var addAFloor = false;
    var addADefinition = false;
    for (var i=0; i<fields.length; i++) {
        var field = fields[i];
        var input = $('input[type="text"][name="'+field.name+'"]');
        if (input.length == 1) {
            input.val(field.value);
        } else {
            var select = $('select[name="'+field.name+'"]');
            if (select.length == 1) {
                select.find('option[value="'+field.value+'"]').prop('selected', true);
            } else {
                var checkbox = $('input[type="checkbox"][name="'+field.name+'"]');
                if (checkbox.length == 1) {
                    if (typeof field.value === "boolean") {
                        checkbox.prop('checked', field.value);
                    } else if (field.value === "true") {
                        checkbox.prop('checked',true);
                    } else {
                        checkbox.prop('checked',false);
                    }   
                } else {
                    if (field.name.contains("[floors]")) {
                        addAFloor = true;
                    }
                    if (field.name.contains("[frames]")) {
                        addADefinition = true;
                    }
                    fieldsNotAddedYet.push(field);
                }
            }
        }
    }
    if (addAFloor) {   
        addFloor();
    }
    if (addADefinition) {
        number = addFloorIndicatorDefinition();
        $('input[type="checkbox"]#floor'+number+'to-button').prop('checked',false);
        $('input[type="checkbox"]#floor'+number+'from-button').prop('checked',false);
    }
    if (fieldsNotAddedYet.length>0) {
        if (fieldsNotAddedYet.length != fields.length) {
            setFormFields(fieldsNotAddedYet,i);
        } else {
            for (var i=0; i<fieldsNotAddedYet.length; i++) {
                var field = fieldsNotAddedYet[i];
                console.log("Error: could not import field \"" + field.name + "\" with value \"" + field.value + "\"");
            }
        }
    }
}
//Generate a field array recursivly from a Javascript objects
function recursivlyGetFields(fieldArray,element,fieldNameBase) {
    for (var key in element) {
        if (element.hasOwnProperty(key)) {
            var obj = element[key]; 
            if (typeof obj == "string" || typeof obj == "boolean" || typeof obj == "number") { 
                fieldArray.push({name:(fieldNameBase + '[' + key + ']'),value:obj});
            } else if (typeof element == "object") {
                recursivlyGetFields(fieldArray,obj,fieldNameBase + '[' + key + ']');
            } else {
                console.log("Error while retrieving field:" + key + ":" + obj);
            }   
        }
    }
    return fieldArray;
}

//Save the form in json and show the result in a div
function saveForm() {
    var json = $.toJSON($('#generator-form').serializeObject());
    $('#save-data').html(json);
    $('#dialog-save').dialog("open");
}

//Import the form from user defined json
function importForm() {
    if(!('contains' in String.prototype)) {
       String.prototype.contains = function(str, startIndex) {
                return -1 !== String.prototype.indexOf.call(this, str, startIndex);
       };
    }
    var jsonResult = $.secureEvalJSON($('#import-data').val());
    var fields = [];
    fields = recursivlyGetFields(fields,jsonResult.generator,"generator");
    setFormFields(fields);
    $('select').change();
    $('input[type="checkbox"]').change();
    $('#dialog-import').dialog("close");
}
function detailsButton() {
    if($('#result-div').css('display') == 'none') {
        $('#result-div').stop().fadeIn();
        $('#details-button').html('<i class="fa fa-minus-square"></i> Hide commands details');
    } else {
        $('#result-div').stop().fadeOut();
        $('#details-button').html('<i class="fa fa-plus-square"></i> Show commands details');
    }
}